export let statsMap = {
    ac: "Armor Class",
    str: "Strength",
    int: "Intelligence",
    dex: "Dexerity",
    con: "Constitution",
    wis: "Wisdom",
    cha: "Charisma"
};