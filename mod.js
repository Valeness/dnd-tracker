
function genModMap() {
    let mod = -5;
    let start_index = 2;
    let map = {
        1: -5
    }

    for (let i = start_index; i <= 30; i++) {

        if (i % 2 === 0) {
            mod++;
        }

        map[i] = mod;
    }

    return map;
}

export let modMap = genModMap();