import { writable } from 'svelte/store';

export const current_creatures = writable([]);
export const creature_list = writable([]);
